a = {
    "_id": "5cd84eaea54d75d81a94ba0a",
    "affiliations": [
        {
            "_id": "5cd84a1fa54d75d1bb45c751",
            "name": "JAB",
            "slug": "jab"
        }
    ],
    "affiliations_id": [
        "5cd84a1fa54d75d1bb45c751"
    ],
    "celestial_objects": {
        "_id": "5cd9d1fd2fa5dbd585cbdbd9",
        "affiliations": [ // This affiliation is correctly filed
            {
                "_id": "5cd84a1fa54d75d1bb45c751",
                "name": "JAB",
                "slug": "jab"
            }
        ],
        "affiliations_id": [
            "5cd84a1fa54d75d1bb45c751",
            "5cd84a1fa54d75d1bb45c751"
        ],
        "coordinates": {
            "spheric": {
                "latitude": 53.30077479951012,
                "longitude": 63.43494882292201,
                "radius": 3.7416573867739413
            },
            "cartesian": {
                "x": 1,
                "y": 2,
                "z": 3
            },
            "polar": {
                "azimuth": 63.43494882292201,
                "polar": 36.69922520048988,
                "radius": 3.7416573867739413
            }
        },
        "designation": "Earth planet",
        "jump_points_id": [],
        "lore": {
            "danger": 0,
            "economy": 0,
            "habitable": false,
            "population": 0
        },
        "name": "Earth",
        "objectType": "PLANET",
        "size": 0,
        "slug": "earth"
    },
    "celestial_objects_id": [
        "5cd9d1fd2fa5dbd585cbdbd9",
        "5cd9d1fd2fa5dbd585cbdbd9"
    ],
    "coordinates": {
        "Spheric": {
            "latitude": 35.264389682754654,
            "longitude": 45,
            "radius": 173.20508075688772
        },
        "cartesian": {
            "x": 100,
            "y": 100,
            "z": 100
        },
        "polar": {
            "azimuth": 45,
            "polar": 54.735610317245346,
            "radius": 173.20508075688772
        }
    },
    "name": "Solar system",
    "slug": "solar-system"
}