package generic

import (
	"net/http"

	"orbital-pathfinder/src/tools"
)

func SendBadRequest(w http.ResponseWriter) {
	tools.SendApiResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
}
