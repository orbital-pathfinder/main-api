package generic

import (
	"net/http"

	"orbital-pathfinder/src/tools"
)

func SendForbidden(w http.ResponseWriter) {
	tools.SendApiResponse(w, http.StatusForbidden, http.StatusText(http.StatusForbidden), nil)
}
