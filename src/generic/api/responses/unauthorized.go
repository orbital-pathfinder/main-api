package generic

import (
	"net/http"

	"orbital-pathfinder/src/tools"
)

func SendUnauthorized(w http.ResponseWriter) {
	tools.SendApiResponse(w, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
}
