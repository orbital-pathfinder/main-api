package constants

const (
	Timeout       = 10
	MongoAddress  = "MONGODB_URI"
	MongoDatabase = "MONGODB_DATABASE"
	MongoUsername = "MONGODB_USERNAME"
	MongoPassword = "MONGODB_PASSWORD"

	SystemsCollection          = "systems"
	AffiliationsCollection     = "affiliations"
	CelestialObjectsCollection = "celestial_objects"
	JumpPointsCollection       = "jump_points"
	TokensCollection           = "tokens"
	UsersCollection            = "users"
)
