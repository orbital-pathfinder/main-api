package constants

const (
	TokenCookieName     = "token"
	TokenCookieSecure   = false
	TokenCookieHttpOnly = false
	TokenCookiePath     = "/"
	TokenMaxAge         = 30 * 24 * 60 * 60

	AuthenticatedCookieName    = "authenticated"
	AuthentifiedCookieSecure   = false
	AuthentifiedCookieHttpOnly = false
	AuthentifiedCookiePath     = TokenCookiePath
	AuthenticationMaxAge       = TokenMaxAge
)
