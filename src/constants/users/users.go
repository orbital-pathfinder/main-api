package constants

const (
	DefaultUserRole   = "ROLE_USER"
	DefaultModoRole   = "ROLE_MODERATOR"
	DefaultAdminRole  = "ROLE_ADMIN"
	DefaultMasterRole = "ROLE_MASTER"
)
