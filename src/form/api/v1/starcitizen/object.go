package form

type Object struct {
	ObjectType   string          `json:"object_type"`
	Designation  string          `json:"designation"`
	Name         string          `json:"name"`
	Coordinates  Coordinates     `json:"coordinates"`
	Size         float64         `json:"size"`
}
