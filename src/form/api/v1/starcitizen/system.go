package form

type CreateSystem struct {
	Name         string      `json:"name"`
	Coordinates  Coordinates `json:"coordinates"`
	Affiliations []string    `json:"affiliations,omitempty"`
}
