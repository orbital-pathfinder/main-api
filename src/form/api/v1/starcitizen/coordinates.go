package form

type Cartesian struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`
}

type Polar struct {
	Radius  float64 `json:"radius"`
	Polar   float64 `json:"polar"`
	Azimuth float64 `json:"azimuth"`
}

type Spheric struct {
	Radius    float64 `json:"radius"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type Coordinates struct {
	Cartesian *Cartesian `json:"cartesian,omitempty"`
	Polar     *Polar     `json:"polar,omitempty"`
	Spheric   *Spheric   `json:"Spheric,omitempty"`
}
