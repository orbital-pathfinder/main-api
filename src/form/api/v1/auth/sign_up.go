package form

type SignUp struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Mail     string `json:"mail,omitempty"`
}
