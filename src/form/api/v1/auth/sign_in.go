package form

type SignIn struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	Remind   bool   `json:"remind"`
}
