package router

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

type Router struct {
	Basepath string
	routes   map[string]http.HandlerFunc
}

func NewRouter(basepath string) *Router {
	ret := &Router{Basepath: basepath}
	ret.routes = map[string]http.HandlerFunc{}
	return ret
}

func (r *Router) Register(path string, middlewares ...tools.MdFunc) {

	handler := tools.MiddlewareResolver(middlewares)

	r.routes[r.Basepath+path] = func(w http.ResponseWriter, r *http.Request) {
		rro := &entities.RequestResolvedOptions{
			Session: &entities.Session{},
		}
		handler(rro, w, r)
	}
}

func (r *Router) Use(router *Router) {
	for path, handler := range router.routes {
		r.routes[ r.Basepath+path ] = handler
	}
}

func (r *Router) Apply(mux *mux.Router) {
	for path, handler := range r.routes {
		fmt.Println("applying route", path)
		mux.HandleFunc(path, handler)
	}
}
