package security

import "gopkg.in/square/go-jose.v2/jwt"

func ParseJwt(raw string) (*Claims, error) {
	token, err := jwt.ParseSigned(raw)
	if err != nil {
		return nil, err
	}

	claims := &Claims{}
	err = token.UnsafeClaimsWithoutVerification(claims)
	if err != nil {
		return nil, err
	}

	return claims, nil
}
