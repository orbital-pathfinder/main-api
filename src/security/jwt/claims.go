package security

import (
	"time"

	"gopkg.in/mgo.v2/bson"
	"gopkg.in/square/go-jose.v2/jwt"

	"github.com/satori/go.uuid"

	securityconst "orbital-pathfinder/src/constants/security"
	"orbital-pathfinder/src/models"
)

type Claims struct {
	Issuer   string           `json:"iss,omitempty"`
	Subject  string           `json:"sub,omitempty"`
	Expiry   *jwt.NumericDate `json:"exp,omitempty"`
	IssuedAt *jwt.NumericDate `json:"iat,omitempty"`
	ID       string           `json:"jti,omitempty"`
	UserId   bson.ObjectId    `json:"user_id"`
	UserName string           `json:"user_name"`
}

func CreateClaims(user models.User) Claims {
	return Claims{
		Issuer:   securityconst.JWTIssuer,
		Subject:  securityconst.JwtDefaultSubject,
		Expiry:   jwt.NewNumericDate(time.Now().AddDate(0, 0, securityconst.AuthenticationMaxAge)),
		IssuedAt: jwt.NewNumericDate(time.Now()),
		ID:       uuid.NewV4().String(),
		UserId:   user.Id,
		UserName: user.Name,
	}
}
