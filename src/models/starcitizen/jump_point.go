package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type ScItem struct {
	Id      bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Name    bson.ObjectId `bson:"name" json:"name"`
	Buyable bool          `bson:"buyable" json:"buyable"`
}

type DatePrice struct {
	Id    bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Price float32       `bson:"price" json:"price"`
	Date  time.Time     `bson:"date" json:"date"`
}

type ScTradableItem struct {
	Id           bson.ObjectId `bson:"_id" json:"id,omitempty"`
	ItemId       bson.ObjectId `bson:"itemId" json:"item_id"`
	Price        float32       `bson:"price" json:"price"`
	PriceHistory []DatePrice   `bson:"priceHistory" json:"price_history"`
}

type ScJumpPoint struct {
	Id              bson.ObjectId   `bson:"_id" json:"id,omitempty"`
	Name            string          `bson:"name" json:"name"`
	Coordinates     Coordinates     `bson:"coordinates" json:"coordinates"`
	IsOrbital       bool            `bson:"isOrbital" json:"is_orbital"`
	IsJumpable      bool            `bson:"isJumpable" json:"is_jumpable"`
	Code            string          `bson:"code" json:"code"`
	BuyableItemsId  []bson.ObjectId `bson:"buyableItemsIds" json:"buyable_items,omitempty"`
	BuyableItems    []ScTradableItem  `bson:"-" json:"buyable_items,omitempty"`
	SellableItemsId []bson.ObjectId `bson:"sellableItemsIds" json:"sellable_items,omitempty"`
	SellableItems   []ScTradableItem  `bson:"-" json:"sellable_items,omitempty"`
}
