package models

import "math"

type Transformable interface {
}

type Cartesian struct {
	X float64 `bson:"x" json:"x"`
	Y float64 `bson:"y" json:"y"`
	Z float64 `bson:"z" json:"z"`
}

func DegToRad(deg float64) float64 {
	return deg * math.Pi / 180
}

func RadToDeg(rad float64) float64 {
	return rad * 180 / math.Pi
}

func (cartesian Cartesian) ToPolar() Polar {
	radius := math.Sqrt(cartesian.X*cartesian.X + cartesian.Y*cartesian.Y + cartesian.Z*cartesian.Z)
	return Polar{
		Radius:  radius,
		Polar:   RadToDeg(math.Acos(cartesian.Z / radius)),
		Azimuth: RadToDeg(math.Atan2(cartesian.Y, cartesian.X)),
	}
}

func (cartesian Cartesian) ToSpheric() Spheric {
	radius := math.Sqrt(cartesian.X*cartesian.X + cartesian.Y*cartesian.Y + cartesian.Z*cartesian.Z)
	return Spheric{
		Radius:    radius,
		Latitude:  RadToDeg(math.Asin(cartesian.Z / radius)),
		Longitude: RadToDeg(math.Atan2(cartesian.Y, cartesian.X)),
	}
}

type Polar struct {
	Radius  float64 `bson:"radius" json:"radius"`
	Polar   float64 `bson:"polar" json:"polar"`
	Azimuth float64 `bson:"azimuth" json:"azimuth"`
}

func (polar Polar) ToSpheric() Spheric {
	return Spheric{
		Radius:    polar.Radius,
		Longitude: polar.Azimuth,
		Latitude:  polar.Polar - 90,
	}
}

func (polar Polar) ToCartesian() Cartesian {
	return Cartesian{
		X: polar.Radius * math.Sin(DegToRad(polar.Polar)) * math.Cos(DegToRad(polar.Azimuth)),
		Y: polar.Radius * math.Sin(DegToRad(polar.Polar)) * math.Sin(DegToRad(polar.Azimuth)),
		Z: polar.Radius * math.Cos(DegToRad(polar.Polar)),
	}
}

type Spheric struct {
	Radius    float64 `bson:"radius" json:"radius"`
	Longitude float64 `bson:"longitude" json:"longitude"`
	Latitude  float64 `bson:"latitude" json:"latitude"`
}

func (Spheric Spheric) ToPolar() Polar {
	return Polar{
		Radius:  Spheric.Radius,
		Azimuth: Spheric.Longitude,
		Polar:   Spheric.Latitude + 90,
	}
}

// x = radius * cos(latitude) * cos(longitude)
// y = radius * cos(latitude) * sin(longitude)
// z = radius * sin(latitude)

func (Spheric Spheric) ToCartesian() Cartesian {
	return Cartesian{
		X: Spheric.Radius * math.Cos(DegToRad(Spheric.Latitude)) * math.Cos(DegToRad(Spheric.Longitude)),
		Y: Spheric.Radius * math.Cos(DegToRad(Spheric.Latitude)) * math.Sin(DegToRad(Spheric.Longitude)),
		Z: Spheric.Radius * math.Sin(DegToRad(Spheric.Latitude)),
	}
}

type Coordinates struct {
	Cartesian Cartesian `bson:"cartesian" json:"cartesian,omitempty"`
	Polar     Polar     `bson:"polar" json:"polar,omitempty"`
	Spheric   Spheric   `bson:"spheric" json:"spheric,omitempty"`
}
