package models

import "gopkg.in/mgo.v2/bson"

type ScAffiliation struct {
	Id   bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Name string        `bson:"name" json:"name"`
	Slug string        `bson:"slug" json:"slug"`
}
