package models

import (
	"gopkg.in/mgo.v2/bson"
)

type ScSystem struct {
	Id                 bson.ObjectId       `bson:"_id" json:"id,omitempty"`
	Name               string              `bson:"name" json:"name"`
	Slug               string              `bson:"slug" json:"slug"`
	Coordinates        Coordinates         `bson:"coordinates" json:"coordinates"`
	AffiliationsId     []bson.ObjectId     `bson:"affiliations_id" json:"affiliations_id,omitempty"`
	Affiliations       []ScAffiliation     `bson:"affiliations" json:"affiliations,omitempty"`
	CelestialObjectsId []bson.ObjectId     `bson:"celestial_objects_id" json:"celestial_objects_id,omitempty"`
	CelestialObjects   []ScCelestialObject `bson:"celestial_objects" json:"celestial_objects,omitempty"`
}
