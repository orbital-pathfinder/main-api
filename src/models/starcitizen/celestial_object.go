package models

import (
	"gopkg.in/mgo.v2/bson"
)

type ScCelestialObjectLore struct {
	Habitable  bool `bson:"habitable" json:"habitable"`
	Population int  `bson:"population" json:"population"`
	Economy    int  `bson:"economy" json:"economy"`
	Danger     int  `bson:"danger" json:"danger"`
}

type ScCelestialObject struct {
	Id             bson.ObjectId         `bson:"_id" json:"id,omitempty"`
	ObjectType     string                `bson:"objectType" json:"object_type"`
	Designation    string                `bson:"designation" json:"designation"`
	Name           string                `bson:"name" json:"name"`
	Slug           string                `bson:"slug" json:"slug"`
	Coordinates    Coordinates           `bson:"coordinates" json:"coordinates"`
	Lore           ScCelestialObjectLore `bson:"lore" json:"lore"`
	Size           float64               `bson:"size" json:"size"`
	AffiliationIds []bson.ObjectId       `bson:"affiliations_id" json:"affiliation_ids,omitempty"`
	Affiliations   []ScAffiliation       `bson:"affiliations" json:"affiliations,omitempty"`
	JumpPointsIds  []bson.ObjectId       `bson:"jump_points_id" json:"jump_points_ids,omitempty"`
	JumpPoints     []ScJumpPoint         `bson:"jump_points" json:"jump_points,omitempty"`
}
