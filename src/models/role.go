package models

import "gopkg.in/mgo.v2/bson"

type Role struct {
	Id   bson.ObjectId `bson:"_id" json:"-"`
	Name string        `bson:"name" json:"name"`
}
