package models

import (
	"gopkg.in/mgo.v2/bson"

	// . "orbital-pathfinder/src/repository/roles"
)

type User struct {
	Id    bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Name  string        `bson:"name" json:"name"`
	Roles []string      `bson:"roles" json:"roles"`
	Hash  string        `bson:"hash" json:"-"`
	Theme string        `bson:"theme" json:"theme"`
}
