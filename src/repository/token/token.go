package repository

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models"
	. "orbital-pathfinder/src/repository"
	. "orbital-pathfinder/src/repository/user"

	dbconsts "orbital-pathfinder/src/constants/database"
)

type DbTokens struct{}

var TokenRepository DbTokens

var collection *mgo.Collection

func init() {
	TokenRepository = DbTokens{}
	TokenRepository.Connect()
}

func (*DbTokens) Connect() {
	collection = Db.C(dbconsts.TokensCollection)
}

func (*DbTokens) Find() ([]models.TokenInfo, error) {
	tokens := []models.TokenInfo{}

	err := collection.Find(bson.M{}).All(&tokens)
	if err != nil {
		return nil, err
	}
	return tokens, nil
}

func (dbtkn *DbTokens) FindById(id string) (*models.TokenInfo, error) {
	return dbtkn.FindByObjectId(bson.ObjectIdHex(id))
}

func (*DbTokens) FindByObjectId(id bson.ObjectId) (*models.TokenInfo, error) {
	token := &models.TokenInfo{}

	err := collection.Find(id).One(token)
	if err != nil {
		return nil, err
	}
	return token, nil
}

type Token struct {
	Id       bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Uuid     string        `bson:"uuid" json:"uuid"`
	Expiry   time.Time     `bson:"expiry" json:"expiry"`
	IssuedAt time.Time     `bson:"issued_at" json:"issued_at"`
	UserId   bson.ObjectId `bson:"user_id" json:"user_id"`
	User     models.User   `bson:"user" json:"user"`
}

func (*DbTokens) FindByUUIDTest(uuid *uuid.UUID) *Token {
	token := &Token{}

	_ = collection.Pipe([]bson.M{
		{
			"$match": bson.M{
				"uuid": uuid.String(),
			},
		},
		{
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "user_id",
				"foreignField": "_id",
				"as":           "user",
			},
		},
	}).One(token)

	return token
}

func (*DbTokens) FindByUUID(uuid *uuid.UUID) (*models.TokenInfo, error) {
	token := &models.TokenInfo{}

	err := collection.Find(bson.M{"uuid": uuid.String()}).One(&token)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (*DbTokens) Insert(info *models.TokenInfo) (*models.TokenInfo, error) {
	tkn := *info;
	tkn.User = nil
	err := collection.Insert(tkn)
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (dbtkn *DbTokens) DeleteById(id bson.ObjectId) error {
	return collection.RemoveId(id)
}


func (*DbTokens) ResolveNested(level int, t *models.TokenInfo) *models.TokenInfo {

	if level < 0 || t == nil {
		return t
	}

	usr, err := UserRepository.FindByObjectId(t.UserId)
	if err != nil {
		return t
	}
	if level > 0 {
		_ = UserRepository.ResolveNested(level-1, usr)
	}

	t.User = usr
	return t
}

