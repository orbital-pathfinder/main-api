package repository

import (
	"github.com/avelino/slugify"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models/starcitizen"
	. "orbital-pathfinder/src/repository"

	dbconsts "orbital-pathfinder/src/constants/database"
)

type DbStarcitizenAffiliation struct {
	collection *mgo.Collection
}

var ScAffiliationRepo DbStarcitizenAffiliation

func init() {
	ScAffiliationRepo = DbStarcitizenAffiliation{}
	ScAffiliationRepo.connect()
}

func (db *DbStarcitizenAffiliation) connect() {
	db.collection = Db.C(dbconsts.AffiliationsCollection)
}

// Respond with all affiliations
func (db *DbStarcitizenAffiliation) Find() ([]models.ScAffiliation, error) {
	affiliations := []models.ScAffiliation{}

	err := db.collection.Find(bson.M{}).All(&affiliations)
	if err != nil {
		return nil, err
	}
	return affiliations, nil
}

// Respond with all affiliations corresponding to given ids
func (db *DbStarcitizenAffiliation) FindAllById(ids []bson.ObjectId) ([]models.ScAffiliation, error) {
	affiliations := []models.ScAffiliation{}

	err := db.collection.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&affiliations)
	if err != nil {
		return nil, err
	}
	return affiliations, nil
}

// Respond one affiliation corresponding to the given id (alias for findByObjectId)
func (db *DbStarcitizenAffiliation) FindById(id string) (*models.ScAffiliation, error) {
	return db.FindByObjectId(bson.ObjectIdHex(id))
}

// Respond one affiliation corresponding to the given id
func (db *DbStarcitizenAffiliation) FindByObjectId(id bson.ObjectId) (*models.ScAffiliation, error) {
	affiliation := &models.ScAffiliation{}

	err := db.collection.Find(id).One(affiliation)
	if err != nil {
		return nil, err
	}
	return affiliation, nil
}

// Respond one affiliation corresponding to the given name
func (db *DbStarcitizenAffiliation) FindByName(name string) (*models.ScAffiliation, error) {
	affiliation := &models.ScAffiliation{}

	err := db.collection.Find(bson.M{"name": name}).One(affiliation)
	if err != nil {
		return nil, err
	}
	return affiliation, nil
}

// Respond one affiliation corresponding to the given slug
func (db *DbStarcitizenAffiliation) FindBySlug(name string) (*models.ScAffiliation, error) {
	affiliation := &models.ScAffiliation{}

	err := db.collection.Find(bson.M{"slug": name}).One(affiliation)
	if err != nil {
		return nil, err
	}
	return affiliation, nil
}

// Respond all records corresponding to the uniques identifiers contained in strings
func (db *DbStarcitizenAffiliation) FindAllByUniqueKeys(keys []string) ([]models.ScAffiliation, error) {
	affiliations := []models.ScAffiliation{}

	for _, key := range keys {
		affiliation := &models.ScAffiliation{}
		err := db.collection.Find(bson.M{"$or": []bson.M{{"_id": key}, {"name": key}, {"slug": key}}}).One(affiliation)
		if err != nil {
			return nil, err
		}
		affiliations = append(affiliations, *affiliation)
	}
	return affiliations, nil
}

// Insert the given affiliation
func (db *DbStarcitizenAffiliation) Insert(affiliation *models.ScAffiliation) error {

	if affiliation.Slug == "" {
		affiliation.Slug = slugify.Slugify(affiliation.Name)
	}
	if err := db.collection.Insert(affiliation); err != nil {
		return err
	}
	return nil
}

