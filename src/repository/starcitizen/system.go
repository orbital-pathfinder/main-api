package repository

import (
	"fmt"

	"github.com/avelino/slugify"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models/starcitizen"
	. "orbital-pathfinder/src/repository"

	dbconsts "orbital-pathfinder/src/constants/database"
)

type DbStarcitizenSystem struct {
	collection *mgo.Collection
}

var ScSystemRepo DbStarcitizenSystem

func init() {
	ScSystemRepo = DbStarcitizenSystem{}
	ScSystemRepo.connect()
}

func (db *DbStarcitizenSystem) connect() {
	db.collection = Db.C(dbconsts.SystemsCollection)
}

func (db *DbStarcitizenSystem) Detailed() *Query {
	q := CreateQuery(db.collection).AddItem(bson.M{
		"$lookup": bson.M{
			"from": "celestial_objects",
			"pipeline": []bson.M{
				{
					"$lookup": bson.M{
						"from":         "affiliations",
						"localField":   "affiliations_id",
						"foreignField": "_id",
						"as":           "affiliations",
					},
				},
			},
			"as": "celestial_objects",
		},
	})
	q.SimpleLookup("affiliations")
	q.RemoveFields("affiliations_id", "celestial_objects_id", "celestial_objects.affiliations_id")
	return q
}

func (db *DbStarcitizenSystem) FindAll() ([]models.ScSystem, error) {
	var ret []models.ScSystem

	query := db.Detailed()
	err := query.All(&ret)
	fmt.Println(ret)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *DbStarcitizenSystem) matchDetailedBy(m bson.M) *mgo.Pipe {
	query := []bson.M{
		m,
		{
			"$lookup": bson.M{
				"from": "celestial_objects",
				"let": bson.M{
					"co": "$celestial_objects_id",
				},
				"pipeline": []bson.M{
					{
						"$lookup": bson.M{
							"from":         "affiliations",
							"localField":   "affiliations_id",
							"foreignField": "_id",
							"as":           "affiliations",
						},
					},
				},
				"as": "celestial_objects",
			},
		},
		{
			"$lookup": bson.M{
				"from":         "affiliations",
				"localField":   "affiliations_id",
				"foreignField": "_id",
				"as":           "affiliations",
			},
		},
		{
			"$project": bson.M{
				"affiliations_id":                   0,
				"celestial_objects_id":              0,
				"celestial_objects.affiliations_id": 0,
			},
		},
	}
	pipe := db.collection.Pipe(query)
	return pipe
}

func (db *DbStarcitizenSystem) FindById(id string) (*models.ScSystem, error) {
	ret := &models.ScSystem{}

	err := CreateQuery(db.collection).Match("de").Eq(bson.ObjectIdHex(id)).One(ret)
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (db *DbStarcitizenSystem) FindByIdDetailed(id string) (*models.ScSystem, error) {
	return db.FindByObjectIdDetailed(bson.ObjectIdHex(id))
}

func (db *DbStarcitizenSystem) FindByObjectIdDetailed(id bson.ObjectId) (*models.ScSystem, error) {
	ret := &models.ScSystem{}

	query := bson.M{
		"$match": bson.M{
			"_id": bson.M{
				"$eq": id,
			},
		},
	}

	err := db.matchDetailedBy(query).One(&ret)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *DbStarcitizenSystem) FindByObjectId(id bson.ObjectId) (*models.ScSystem, error) {
	system := &models.ScSystem{}

	err := db.collection.Find(id).One(system)
	if err != nil {
		return nil, err
	}
	return system, nil
}

func (db *DbStarcitizenSystem) FindDetailedByName(name string) (*models.ScSystem, error) {
	system := &models.ScSystem{}

	err := db.matchDetailedBy(bson.M{
		"$match": bson.M{
			"name": bson.M{
				"$eq": name,
			},
		},
	}).One(&system)
	if err != nil {
		return nil, err
	}
	return system, nil
}

func (db *DbStarcitizenSystem) FindByName(name string) (*models.ScSystem, error) {
	system := &models.ScSystem{}

	err := db.collection.Find(bson.M{"name": name}).One(system)
	if err != nil {
		return nil, err
	}
	return system, nil
}

func (db *DbStarcitizenSystem) FindDetailedBySlug(slug string) (*models.ScSystem, error) {
	system := &models.ScSystem{}

	err := db.matchDetailedBy(bson.M{
		"$match": bson.M{
			"slug": bson.M{"$eq": slug},
		},
	}).One(system)
	if err != nil {
		return nil, err
	}
	return system, nil
}

func (db *DbStarcitizenSystem) FindBySlug(name string) (*models.ScSystem, error) {
	system := &models.ScSystem{}

	err := db.collection.Find(bson.M{"slug": name}).One(system)
	if err != nil {
		return nil, err
	}
	return system, nil
}
func (db *DbStarcitizenSystem) RemoveAffiliationByName(system *models.ScSystem, name string) error {
	affiliation, err := ScAffiliationRepo.FindByName(name)
	if err != nil {
		return err
	}
	ids := []bson.ObjectId{}
	for _, id := range system.AffiliationsId {
		if id != affiliation.Id {
			ids = append(ids, id)
		}
	}
	system.AffiliationsId = ids
	return nil
}

func (db *DbStarcitizenSystem) AddAffiliationByName(system *models.ScSystem, name string) error {
	affiliation, err := ScAffiliationRepo.FindByName(name)
	if err != nil {
		return err
	}
	for _, id := range system.AffiliationsId {
		if id == affiliation.Id {
			return nil
		} else {
			system.AffiliationsId = append(system.AffiliationsId, affiliation.Id)
		}
	}
	return nil
}

func (db *DbStarcitizenSystem) Insert(system *models.ScSystem) error {
	if system.Slug == "" {
		system.Slug = slugify.Slugify(system.Name)
	}
	if err := db.collection.Insert(system); err != nil {
		return err
	}
	return nil
}

func (db *DbStarcitizenSystem) Update(system *models.ScSystem) error {
	if system.Slug == "" {
		system.Slug = slugify.Slugify(system.Name)
	}
	if err := db.collection.Update(bson.M{"_id": system.Id}, system); err != nil {
		return err
	}
	return nil
}
