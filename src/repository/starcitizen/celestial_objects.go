package repository

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models/starcitizen"
	. "orbital-pathfinder/src/repository"

	dbconsts "orbital-pathfinder/src/constants/database"
)

type DbStarcitizenCelestialObject struct {
	collection *mgo.Collection
}

var ScCelestialObjectRepo DbStarcitizenCelestialObject

func init() {
	ScCelestialObjectRepo = DbStarcitizenCelestialObject{}
	ScCelestialObjectRepo.connect()
}

func (db *DbStarcitizenCelestialObject) connect() {
	db.collection = Db.C(dbconsts.CelestialObjectsCollection)
}

func (db *DbStarcitizenCelestialObject) Find() ([]models.ScCelestialObject, error) {
	celestialObjects := []models.ScCelestialObject{}

	err := db.collection.Find(bson.M{}).All(&celestialObjects)
	if err != nil {
		return nil, err
	}
	return celestialObjects, nil
}

func (db *DbStarcitizenCelestialObject) FindById(id string) (*models.ScCelestialObject, error) {
	return db.FindByObjectId(bson.ObjectIdHex(id))
}

func (db *DbStarcitizenCelestialObject) FindByObjectId(id bson.ObjectId) (*models.ScCelestialObject, error) {
	celestialObject := &models.ScCelestialObject{}

	err := db.collection.Find(id).One(celestialObject)
	if err != nil {
		return nil, err
	}
	return celestialObject, nil
}

// Respond with all affiliations corresponding to given ids
func (db *DbStarcitizenCelestialObject) FindAllById(ids []bson.ObjectId) ([]models.ScCelestialObject, error) {
	objects := []models.ScCelestialObject{}

	err := db.collection.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&objects)
	if err != nil {
		return nil, err
	}
	return objects, nil
}

func (db *DbStarcitizenCelestialObject) FindByName(name string) (*models.ScCelestialObject, error) {
	celestialObject := &models.ScCelestialObject{}

	err := db.collection.Find(bson.M{"name": name}).One(celestialObject)
	if err != nil {
		return nil, err
	}
	return celestialObject, nil
}

func (db *DbStarcitizenCelestialObject) FindBySlug(name string) (*models.ScCelestialObject, error) {
	celestialObject := &models.ScCelestialObject{}

	err := db.collection.Find(bson.M{"slug": name}).One(celestialObject)
	if err != nil {
		return nil, err
	}
	return celestialObject, nil
}

func (db *DbStarcitizenCelestialObject) Insert(celestialObject *models.ScCelestialObject) error {
	if err := db.collection.Insert(celestialObject); err != nil {
		return err
	}
	return nil
}

func (db *DbStarcitizenCelestialObject) FindBySystemSlug(systemSlug string) ([]models.ScCelestialObject, error) {
	any := []interface{}{}
	celestialObjects := []models.ScCelestialObject{}

	query := QueryFindBySystemSlug(systemSlug)

	err := ScSystemRepo.collection.Pipe(query).All(&celestialObjects)
	err = ScSystemRepo.collection.Pipe(query).All(&any)
	if err != nil {
		return nil, err
	}

	return celestialObjects, nil
}
