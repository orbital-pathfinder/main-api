package repository

import (
	"gopkg.in/mgo.v2"

	models "orbital-pathfinder/src/models/starcitizen"
)

type DbStarcitizenCoordinates struct {
	collection *mgo.Collection
}

var ScCoordinatesRepo DbStarcitizenCoordinates

func init() {
	ScCoordinatesRepo = DbStarcitizenCoordinates{}
}

func (db *DbStarcitizenCoordinates) CalculateFromSpheric(Spheric models.Spheric) models.Coordinates {
	ret := models.Coordinates{}

	ret.Spheric = Spheric
	ret.Polar = ret.Spheric.ToPolar()
	ret.Cartesian = ret.Spheric.ToCartesian()

	return ret
}

func (db *DbStarcitizenCoordinates) CalculateFromPolar(polar models.Polar) models.Coordinates {
	ret := models.Coordinates{}

	ret.Polar = polar
	ret.Spheric = ret.Polar.ToSpheric()
	ret.Cartesian = ret.Polar.ToCartesian()

	return ret
}

func (db *DbStarcitizenCoordinates) CalculateFromCartesian(cartesian models.Cartesian) models.Coordinates {
	ret := models.Coordinates{}

	ret.Cartesian = cartesian
	ret.Polar = ret.Cartesian.ToPolar()
	ret.Spheric = ret.Cartesian.ToSpheric()

	return ret
}
