package repository

import "gopkg.in/mgo.v2/bson"

func QueryFindBySystemSlug(systemSlug string) []bson.M {
	return []bson.M{
		{
			"$match": bson.M{
				"slug": bson.M{
					"$eq": systemSlug,
				},
			},
		},
		{
			"$lookup": bson.M{
				"from": "celestial_objects",
				"let": bson.M{
					"ids": "$celestial_objects_id",
				},
				"pipeline": []bson.M{
					{
						"$match": bson.M{
							"$expr": bson.M{
								"$in": []string{"$_id", "$$ids"},
							},
						},
					},
					{
						"$lookup": bson.M{
							"from":         "affiliations",
							"localField":   "affiliations_id",
							"foreignField": "_id",
							"as":           "affiliations",
						},
					},
				},
				"as": "celestial_objects",
			},
		},
		{
			"$unwind": "$celestial_objects",
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$celestial_objects",
			},
		},
	}
}