package repository

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models/starcitizen"
	. "orbital-pathfinder/src/repository"

	dbconsts "orbital-pathfinder/src/constants/database"
)

type DbStarcitizenJumpPoint struct {
	collection *mgo.Collection
}

var ScJumpPointRepo DbStarcitizenJumpPoint

func init() {
	ScJumpPointRepo = DbStarcitizenJumpPoint{}
	ScJumpPointRepo.connect()
}

func (db *DbStarcitizenJumpPoint) connect() {
	db.collection = Db.C(dbconsts.JumpPointsCollection)
}

// Respond with all jumpPoints
func (db *DbStarcitizenJumpPoint) Find() ([]models.ScJumpPoint, error) {
	jumpPoints := []models.ScJumpPoint{}

	err := db.collection.Find(bson.M{}).All(&jumpPoints)
	return jumpPoints, err
}

// Respond with all jumpPoints corresponding to given ids
func (db *DbStarcitizenJumpPoint) FindAllById(ids []bson.ObjectId) ([]models.ScJumpPoint, error) {
	jumpPoints := []models.ScJumpPoint{}

	err := db.collection.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&jumpPoints)
	return jumpPoints, err
}

// Respond one jumpPoint corresponding to the given id (alias for findByObjectId)
func (db *DbStarcitizenJumpPoint) FindById(id string) (models.ScJumpPoint, error) {
	return db.FindByObjectId(bson.ObjectIdHex(id))
}

// Respond one jumpPoint corresponding to the given id
func (db *DbStarcitizenJumpPoint) FindByObjectId(id bson.ObjectId) (models.ScJumpPoint, error) {
	jumpPoint := models.ScJumpPoint{}

	err := db.collection.Find(id).One(&jumpPoint)
	return jumpPoint, err
}

// Respond one jumpPoint corresponding to the given name
func (db *DbStarcitizenJumpPoint) FindByName(name string) (models.ScJumpPoint, error) {
	jumpPoint := models.ScJumpPoint{}

	err := db.collection.Find(bson.M{"name": name}).One(&jumpPoint)
	return jumpPoint, err
}

// Insert the given jumpPoint
func (db *DbStarcitizenJumpPoint) Insert(jumpPoint models.ScJumpPoint) error {
	if err := db.collection.Insert(&jumpPoint); err != nil {
		return err
	}
	return nil
}

