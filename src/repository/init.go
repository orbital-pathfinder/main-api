package repository

import (
	"log"
	"os"
	"time"

	"gopkg.in/mgo.v2"

	dbconsts "orbital-pathfinder/src/constants/database"
)

var Db *mgo.Database

func init() {
	mgoTimeout := dbconsts.Timeout
	mgoDialInfo := &mgo.DialInfo{
		Addrs:    []string{os.Getenv(dbconsts.MongoAddress)},
		Timeout:  time.Duration(time.Duration(mgoTimeout)) * time.Second,
		Database: os.Getenv(dbconsts.MongoDatabase),
		Username: os.Getenv(dbconsts.MongoUsername),
		Password: os.Getenv(dbconsts.MongoPassword),
	}
	session, err := mgo.DialWithInfo(mgoDialInfo)
	if err != nil {
		log.Panic(err)
	}
	Db = session.DB(os.Getenv(dbconsts.MongoDatabase))
}
