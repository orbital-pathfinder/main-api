package repository

import "gopkg.in/mgo.v2/bson"

type MatchOperand struct {
	q     *Query
	field string
}

func (mo *MatchOperand) Eq(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$eq": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Ne(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$ne": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Gt(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$gt": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Gte(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$gte": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) In(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$in": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Nin(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$nin": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Lt(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$lt": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (mo *MatchOperand) Lte(val interface{}) *Query {
	mo.q.pipeline = append([]bson.M{
		{
			"$match": bson.M{
				mo.field: bson.M{
					"$lte": val,
				},
			},
		},
	}, mo.q.pipeline...)
	return mo.q
}

func (q *Query) Match(field string) *MatchOperand {
	return &MatchOperand{
		q,
		field,
	}
}
