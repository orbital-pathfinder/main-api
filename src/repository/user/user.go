package repository

import (
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	dbconst "orbital-pathfinder/src/constants/database"
	userConst "orbital-pathfinder/src/constants/users"
	entities "orbital-pathfinder/src/form/api/v1/auth"
	"orbital-pathfinder/src/models"
	. "orbital-pathfinder/src/repository"
)

type DbUsers struct {
	collection *mgo.Collection
}

var UserRepository DbUsers

func init() {
	UserRepository = DbUsers{}
	UserRepository.connect()
}

func (dbusr *DbUsers) connect() {
	dbusr.collection = Db.C(dbconst.UsersCollection)
}

func (dbusr *DbUsers) Find() ([]*models.User, error) {
	users := []*models.User{}

	err := dbusr.collection.Find(bson.M{}).All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (dbusr *DbUsers) FindById(id string) (*models.User, error) {
	return dbusr.FindByObjectId(bson.ObjectIdHex(id))
}

func (dbusr *DbUsers) FindByObjectId(id bson.ObjectId) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"_id": id}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByName(name string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"name": name}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByMail(mail string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"mail": mail}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByUsernameOrMail(username string, mail string) (*models.User, error) {
	user := &models.User{}

	err := dbusr.collection.Find(bson.M{"$or": []bson.M{{"name": username}, {"mail": mail}}}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) FindByLogin(login string) (*models.User, error) {
	return dbusr.FindByUsernameOrMail(login, login)
}

func (dbusr *DbUsers) FindByLoginAndPassword(login string, password string) (*models.User, error) {
	user, err := dbusr.FindByLogin(login)

	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Hash), []byte(password))
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (dbusr *DbUsers) CreateUserNoHashPassword(user entities.SignUp) (*models.User, error) {
	insert := &models.User{}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	insert.Name = user.Username
	insert.Id = bson.NewObjectId()
	insert.Hash = string(hash)
	insert.Roles = []string{userConst.DefaultUserRole}

	err = dbusr.collection.Insert(insert)
	if err == nil {
		return nil, err
	}

	return insert, nil
}

func (dbusr *DbUsers) UpdateByID(id bson.ObjectId, user *models.User) error {
	return dbusr.collection.Update(bson.M{"_id": user.Id}, user)

}

func (dbusr *DbUsers) Persist(user *models.User) error {
	return dbusr.UpdateByID(user.Id, user)
}

func (dbusr *DbUsers) ResolveNested(level int, u *models.User) *models.User {
	if level < 0 {
		return u
	}

	return u
}

func (dbusr *DbUsers) ResolveNestedAll(level int, us []*models.User) []*models.User {
	for _, u := range us {
		dbusr.ResolveNested(level, u)
	}

	return us
}
