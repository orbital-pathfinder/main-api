package repository

import (
	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Query struct {
	pipeline   []bson.M
	collection *mgo.Collection
}

func CreateQuery(collection *mgo.Collection) *Query {
	return &Query{
		pipeline:   []bson.M{},
		collection: collection,
	}
}

func (q *Query) FindById(id bson.ObjectId) *Query {
	q.AddItem(bson.M{
		"$match": bson.M{
			"_id": bson.M{
				"$eq": id,
			},
		},
	})
	return q
}

func (q *Query) FindBySlug(slug string) *Query {
	q.AddItem(bson.M{
		"$match": bson.M{
			"slug": bson.M{
				"$eq": slug,
			},
		},
	})
	return q
}

func (q *Query) FindByName(name string) *Query {
	q.AddItem(bson.M{
		"$match": bson.M{
			"name": bson.M{
				"$eq": name,
			},
		},
	})
	return q
}

func (q *Query) AddItem(item bson.M) *Query {
	q.pipeline = append(q.pipeline, item)
	return q
}

func (q *Query) SimpleLookup(from string) *Query {
	return q.FullLookup(from, from+"_id", "_id", from)
}

func (q *Query) FullLookup(from string, local string, foreign string, as string) *Query {
	q.AddItem(bson.M{
		"$lookup": bson.M{
			"from":         from,
			"localField":   local,
			"foreignField": foreign,
			"as":           as,
		},
	})
	return q
}

func (q *Query) AdvancedLoopup(m bson.M) *Query {
	q.AddItem(bson.M{
		"$lookup": m,
	})
	return q
}

func (q *Query) MatchEq(field string, value interface{}) *Query {
	q.AddItem(bson.M{
		"match": bson.M{
			field: bson.M{
				"eq": value,
			},
		},
	})

	return q
}

func (q *Query) Limit(val int) *Query {
	return q.AddItem(bson.M{
		"$limit": val,
	})
}

func (q *Query) Skip(val int) *Query {
	return q.AddItem(bson.M{
		"$skip": val,
	})
}

func (q *Query) All(output interface{}) error {
	return q.collection.Pipe(q.pipeline).All(output)
}

func (q *Query) One(output interface{}) error {
	return q.collection.Pipe(q.pipeline).One(output)
}

func (q *Query) e() *mgo.Iter {
	return q.collection.Pipe(q.pipeline).Iter()
}

func (q *Query) RemoveFields(s ...string) *Query {
	a := bson.M{}
	for _, v := range s {
		a[v] = 0
	}
	return q.AddItem(bson.M{
		"$project": a,
	});
}

func (q *Query) Compare(ms []bson.M) bool {
	for i := 0; i < len(ms); i++ {
		fmt.Println(ms[i])
		fmt.Println(q.pipeline[i])
		fmt.Println("")
	}
	return true
}

func (q *Query) Unwind(field string) *Query {
	q.AddItem(bson.M{
		"$unwind": field,
	})
	return q
}
