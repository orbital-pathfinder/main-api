package roles

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/models"
	. "orbital-pathfinder/src/repository"
)

type DbRoles struct{}

var RoleRepository DbRoles

var collection *mgo.Collection

func init() {
	RoleRepository = DbRoles{}
	RoleRepository.connect()
}

func (*DbRoles) connect() {
	collection = Db.C(database.RolesCollection)
}

func (*DbRoles) Find() ([]models.Role, error) {
	roles := []models.Role{}

	err := collection.Find(bson.M{}).All(&roles)
	return roles, err
}

func (dbrol *DbRoles) FindById(id string) (*models.Role, error) {
	return dbrol.FindByObjectId(bson.ObjectIdHex(id))
}

func (*DbRoles) FindByObjectId(id bson.ObjectId) (*models.Role, error) {
	role := &models.Role{}

	err := collection.Find(id).One(&role)
	return role, err
}

func (*DbRoles) FindByName(name string) (*models.Role, error) {
	role := &models.Role{}

	err := collection.Find(bson.M{"name": name}).One(role)
	return role, err
}

func (*DbRoles) FindAllByObjectId(rolesId []bson.ObjectId) ([]models.Role, error) {
	roles := []models.Role{}

	err := collection.Find(bson.M{"_id": bson.M{"$in": rolesId}}).All(&roles)
	return roles, err
}
