package entities

type Pageable struct {
	Page int `json:"page"`
	Total int `json:"total"`
	TotalPages int `json:"totalPages"`
	Next string `json:"next"`
	Items interface{} `json:"items"`
}