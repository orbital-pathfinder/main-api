package entities

type PagedParams struct {
	Page int
	Size int
}

type RequestResolvedOptions struct {
	Session *Session
	Paged *PagedParams
}