package entities

import "orbital-pathfinder/src/models"

type Session struct {
	Token *models.TokenInfo
}
