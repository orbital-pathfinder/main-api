package users

import (
	"net/http"

	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/users")

	Router.Register("/", CheckMethod(http.MethodGet), IsAuthenticated, HasRole("ROLE_ADMIN"), MultiMethod(Methods{
		http.MethodGet: {GetAllUsersController},
	}))
}
