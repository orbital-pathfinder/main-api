package users

import (
	"net/http"

	"orbital-pathfinder/src/entities"
	. "orbital-pathfinder/src/repository/user"
	"orbital-pathfinder/src/tools"
)

func GetAllUsersController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
	users, _ := UserRepository.Find()

	tools.SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), users)
}
