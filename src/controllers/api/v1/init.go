package v1

import (
	"orbital-pathfinder/src/controllers/api/v1/auth"
	"orbital-pathfinder/src/controllers/api/v1/me"
	affiliation "orbital-pathfinder/src/controllers/api/v1/starcitizen/affiliations"
	"orbital-pathfinder/src/controllers/api/v1/starcitizen/systems"
	"orbital-pathfinder/src/controllers/api/v1/users"
	. "orbital-pathfinder/src/router"
)

var V1Router *Router

func init() {
	V1Router = NewRouter("/v1")
	V1Router.Use(auth.Router)
	V1Router.Use(systems.Router)
	V1Router.Use(users.Router)
	V1Router.Use(me.Router)
	V1Router.Use(affiliation.Router)
}
