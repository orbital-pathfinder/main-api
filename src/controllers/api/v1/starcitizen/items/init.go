package systems

import (
	"net/http"

	constants "orbital-pathfinder/src/constants/users"
	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/items")

	Router.Register("", MultiMethod(Methods{
		http.MethodGet:  {IsAuthenticated, GetAllSystemsController},
		http.MethodPost: {IsAuthenticated, HasRole(constants.DefaultUserRole), HasJsonBody, PostSystemsController},
	}))
}
