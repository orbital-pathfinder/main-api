package systems

import (
	"net/http"

	"orbital-pathfinder/src/entities"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func GetAllItemsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	data, err := repository.ScSystemRepo.FindAll()

	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), err)
		return
	}

	SendApiResponse(w, http.StatusOK, "Ok", data)
}
