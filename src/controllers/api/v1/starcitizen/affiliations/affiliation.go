package affiliation

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	constants "orbital-pathfinder/src/constants/database"
	"orbital-pathfinder/src/entities"
	form "orbital-pathfinder/src/form/api/v1/starcitizen"
	generic "orbital-pathfinder/src/generic/api/responses"
	models "orbital-pathfinder/src/models/starcitizen"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func GetAllAffiliationsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	data, err := repository.ScAffiliationRepo.Find()

	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	SendApiResponse(w, http.StatusOK, "Ok", data)
}

func PostAffiliationController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil)
		return
	}

	// Parsing json body
	formData := form.CreateAffiliation{}
	err = json.Unmarshal(body, &formData)
	if err != nil {
		generic.SendBadRequest(w)
		return
	}

	exist, err := repository.ScAffiliationRepo.FindByName(formData.Name)
	if err != nil {
		switch err.Error() {
		case constants.NotFound:
			break
		default:
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
			return
		}
	}
	if exist != nil {
		SendApiResponse(w, http.StatusConflict, "The affiliation "+formData.Name+" already exist", nil)
		return
	}

	// Filling entity id
	affiliation := &models.ScAffiliation{}
	affiliation.Id = bson.NewObjectId()
	if formData.Name == "" {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a name to the discovered system", nil)
		return
	}

	// Filling entity name
	affiliation.Name = formData.Name

	// Persisting the created affiliation
	if err = repository.ScAffiliationRepo.Insert(affiliation); err != nil {
		SendApiResponse(w, http.StatusInternalServerError, "An error occurred while creating the affiliation.", nil)
		return
	}
	SendApiResponse(w, http.StatusCreated, "Affiliation is created", affiliation)
	return
}
