package affiliation

import (
	"net/http"

	constants "orbital-pathfinder/src/constants/users"
	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/affiliations")

	Router.Register("", MultiMethod(Methods{
		http.MethodGet:  {IsAuthenticated, GetAllAffiliationsController},
		http.MethodPost: {IsAuthenticated, HasRole(constants.DefaultUserRole), HasJsonBody, PostAffiliationController},
	}))
}
