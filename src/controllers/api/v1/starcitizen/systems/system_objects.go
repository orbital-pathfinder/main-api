package systems

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/avelino/slugify"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	"orbital-pathfinder/src/entities"
	form "orbital-pathfinder/src/form/api/v1/starcitizen"
	models "orbital-pathfinder/src/models/starcitizen"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func MakePagedResponse(rro *entities.RequestResolvedOptions, r *http.Request, datas interface{}) entities.Pageable {
	start := (rro.Paged.Page - 1) * rro.Paged.Size
	var slice []interface{}

	if start < len(datas) {
		slice = datas[start:]
	}
	if len(slice) > rro.Paged.Size {
		slice = slice[:rro.Paged.Size]
	}

	totalPages := int((float64(len(datas)) / float64(rro.Paged.Size)) + 0.5)
	_url := *r.URL
	q := _url.Query()
	q.Set("page", strconv.Itoa(rro.Paged.Page + 1))
	q.Set("size", strconv.Itoa(rro.Paged.Size))

	_url.RawQuery = q.Encode()

	fmt.Println(_url.RequestURI())

	return entities.Pageable{
		Page:  rro.Paged.Page,
		Total: len(slice),
		TotalPages: totalPages,
		Next: _url.String(),
		Items: slice,
	}
}

func GetAllSystemObjectsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	objects, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])

	if err != nil {
		fmt.Println(err)
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if objects == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
		return
	}

	if rro.Paged != nil {
		start := (rro.Paged.Page - 1) * rro.Paged.Size
		var slice []models.ScCelestialObject

		if start < len(objects.CelestialObjects) {
			slice = objects.CelestialObjects[start:]
		}
		if len(slice) > rro.Paged.Size {
			slice = slice[:rro.Paged.Size]
		}

		totalPages := int((float64(len(objects.CelestialObjects)) / float64(rro.Paged.Size)) + 0.5)
		_url := *r.URL
		q := _url.Query()
		q.Set("page", strconv.Itoa(rro.Paged.Page + 1))
		q.Set("size", strconv.Itoa(rro.Paged.Size))

		_url.RawQuery = q.Encode()

		fmt.Println(_url.RequestURI())

		data := entities.Pageable{
			Page:  rro.Paged.Page,
			Total: len(slice),
			TotalPages: totalPages,
			Next: _url.String(),
			Items: slice,
		}
		data := MakePagedResponse(rro, r, objects.CelestialObjects)
		SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), data)
	} else {
		SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), objects.CelestialObjects)
	}
	return
}

func PostSystemObjectsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	system, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])
	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if system == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
		return
	}

	formData := []form.Object{}
	if ok := ResolveBody(w, r, &formData); ok == nil {
		return
	}

	for _, entry := range formData {
		coordinate, err := ResolveCoordinates(w, r, entry.Coordinates)
		if err != nil {
			return
		}
		entity := models.ScCelestialObject{
			Id:          bson.NewObjectId(),
			ObjectType:  entry.ObjectType,
			Designation: entry.Designation,
			Name:        entry.Name,
			Slug:        slugify.Slugify(entry.Name),
			Coordinates: *coordinate,
			Lore:        models.ScCelestialObjectLore{},
			Size:        0,
		}
		if err = repository.ScCelestialObjectRepo.Insert(&entity); err != nil {
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
			return
		}
		system.CelestialObjects = append(system.CelestialObjects, entity)
		system.CelestialObjectsId = append(system.CelestialObjectsId, entity.Id)
	}
	err = repository.ScSystemRepo.Update(system)
	fmt.Println(err)

	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system.CelestialObjects)
	return
}
