package systems

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	constants "orbital-pathfinder/src/constants/database"
	"orbital-pathfinder/src/entities"
	form "orbital-pathfinder/src/form/api/v1/starcitizen"
	generic "orbital-pathfinder/src/generic/api/responses"
	models "orbital-pathfinder/src/models/starcitizen"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func GetAllSystemsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	data, err := repository.ScSystemRepo.FindAll()

	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), err)
		return
	}

	SendApiResponse(w, http.StatusOK, "Ok", data)
}

func GetOneSystemController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	// Getting / Checking path vars
	pathParams := mux.Vars(r)
	if _, ok := pathParams["system_slug"]; !ok {
		fmt.Println(time.Now(), "Request has no system_slug", r.URL.String())
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	var system *models.ScSystem
	var err error

	// Looking for the system
	system, err = repository.ScSystemRepo.FindDetailedBySlug(pathParams["system_slug"])
	if err != nil {
		switch err.Error() {
		case "not found":
			SendApiResponse(w, http.StatusNotFound, "System '"+pathParams["system_slug"]+"' is not found", nil)
		default:
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), err)
		}
		return
	}

	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system)
	return
}

func PostSystemsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil)
		return
	}

	// Parsing json body
	formData := form.CreateSystem{}
	err = json.Unmarshal(body, &formData)
	if err != nil {
		generic.SendBadRequest(w)
		return
	}

	exist, err := repository.ScSystemRepo.FindByName(formData.Name)
	if err != nil {
		switch err.Error() {
		case constants.NotFound:
			break
		default:
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
			return
		}
	}
	if exist != nil {
		SendApiResponse(w, http.StatusConflict, "The system "+formData.Name+" already exist", nil)
		return
	}

	// Filling entity id
	system := &models.ScSystem{}
	system.Id = bson.NewObjectId()
	if formData.Name == "" {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a name to the discovered system", nil)
		return
	}

	// Filling entity name
	system.Name = formData.Name

	coord, err := ResolveCoordinates(w, r, formData.Coordinates)
	if err != nil {
		return
	}
	system.Coordinates = *coord;

	// Filling / Solving Affiliations
	system.Affiliations, err = repository.ScAffiliationRepo.FindAllByUniqueKeys(formData.Affiliations)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "At least one of the affiliations is not known", nil)
		return
	}
	for _, affiliation := range system.Affiliations {
		system.AffiliationsId = append(system.AffiliationsId, affiliation.Id)
	}

	// Persisting the created system
	if err = repository.ScSystemRepo.Insert(system); err != nil {
		SendApiResponse(w, http.StatusInternalServerError, "An error occurred while creating the system.", nil)
		return
	}
	SendApiResponse(w, http.StatusCreated, "System is created", system)
	return
}
