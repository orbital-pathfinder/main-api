package systems

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"

	constants "orbital-pathfinder/src/constants/database"
	"orbital-pathfinder/src/entities"
	form "orbital-pathfinder/src/form/api/v1/starcitizen"
	generic "orbital-pathfinder/src/generic/api/responses"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func GetAllSystemAffiliationsController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	system, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])
	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if system == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
	} else {
		SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system.Affiliations)
	}
	return
}

func PostSystemAffiliationController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	system, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])
	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if system == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
		return
	}

	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil)
		return
	}

	// Parsing json body
	formDatas := []string{}
	err = json.Unmarshal(body, &formDatas)
	if err != nil {
		fmt.Println(err)
		generic.SendBadRequest(w)
		return
	}

	for _, formData := range formDatas {
		err := repository.ScSystemRepo.AddAffiliationByName(system, formData)
		if err != nil {
			switch err.Error() {
			case constants.NotFound:
				SendApiResponse(w, http.StatusNotFound, "The affiliation "+formData+" is not found", nil)
			default:
				SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
			}
			return
		}
	}

	if err = repository.ScSystemRepo.Update(system); err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system.Affiliations)
	return
}

func PutSystemAffiliationController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	system, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])
	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if system == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
		return
	}

	// Parsing json body
	formData := &form.CreateAffiliation{}
	if ok := ResolveBody(w, r, formData); ok != nil {
		return
	}

	err = repository.ScSystemRepo.AddAffiliationByName(system, formData.Name)
	if err != nil {
		switch err.Error() {
		case constants.NotFound:
			SendApiResponse(w, http.StatusNotFound, "The affiliation "+formData.Name+" is not found", nil)
		default:
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		}
		return

	}

	if err = repository.ScSystemRepo.Update(system); err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system.Affiliations)
	return
}

func DeleteSystemAffiliationController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	vars := mux.Vars(r)
	system, err := repository.ScSystemRepo.FindDetailedBySlug(vars["system_slug"])
	if err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	if system == nil {
		SendApiResponse(w, http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
		return
	}

	// Parsing json body
	formDatas := []string{}
	ResolveBody(w, r, formDatas)

	// Parsing and adding affiliations
	for _, formData := range formDatas {
		err := repository.ScSystemRepo.RemoveAffiliationByName(system, formData)
		if err != nil {
			switch err.Error() {
			case constants.NotFound:
				SendApiResponse(w, http.StatusNotFound, "The affiliation "+formData+" is not found", nil)
			default:
				SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
			}
			return
		}
	}

	if err := repository.ScSystemRepo.Update(system); err != nil {
		SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), system.Affiliations)
	return
}
