package systems

import (
	"net/http"

	constants "orbital-pathfinder/src/constants/users"
	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/systems")

	Router.Register("", MultiMethod(Methods{
		http.MethodGet:  {GetAllSystemsController},
		http.MethodPost: {IsAuthenticated, HasRole(constants.DefaultUserRole), HasJsonBody, PostSystemsController},
	}))
	Router.Register("/{system_slug}", MultiMethod(Methods{
		http.MethodGet: {GetOneSystemController},
	}))
	Router.Register("/{system_slug}/affiliations", MultiMethod(Methods{
		http.MethodGet:    {GetAllSystemAffiliationsController},
		http.MethodPost:   {IsAuthenticated, PostSystemAffiliationController},
		http.MethodPut:    {IsAuthenticated, HasRole(constants.DefaultModoRole), PutSystemAffiliationController},
		http.MethodDelete: {IsAuthenticated, HasRole(constants.DefaultAdminRole), DeleteSystemAffiliationController},
	}))
	Router.Register("/{system_slug}/objects",
		MultiMethod(Methods{
			http.MethodGet:  {ShouldBePaged(1, 10), GetAllSystemObjectsController},
			http.MethodPost: {IsAuthenticated, PostSystemObjectsController},
			// http.MethodPut: {IsAuthenticated, GetAllSystemObjectsController},
			// http.MethodDelete: {IsAuthenticated, GetAllSystemObjectsController},
		}))
	Router.Register("/{system_slug}/objects/{object_slug}", MultiMethod(Methods{
		// http.MethodGet: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPost: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPut: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodDelete: {IsAuthenticated, GetAllSystemObjectsController},
	}))
	Router.Register("/{system_slug}/objects/{object_slug}/locations", MultiMethod(Methods{
		// http.MethodGet: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPost: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPut: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodDelete: {IsAuthenticated, GetAllSystemObjectsController},
	}))
	Router.Register("/{system_slug}/objects/{object_slug}/locations/{location_slug}", MultiMethod(Methods{
		// http.MethodGet: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPost: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPut: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodDelete: {IsAuthenticated, GetAllSystemObjectsController},
	}))
	Router.Register("/{system_slug}/objects/{object_slug}/locations/{location_slug}/trades", MultiMethod(Methods{
		// http.MethodGet: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPost: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodPut: {IsAuthenticated, GetAllSystemObjectsController},
		// http.MethodDelete: {IsAuthenticated, GetAllSystemObjectsController},
	}))
}
