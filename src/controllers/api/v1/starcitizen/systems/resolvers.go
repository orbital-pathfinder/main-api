package systems

import (
	"fmt"
	"net/http"

	form "orbital-pathfinder/src/form/api/v1/starcitizen"
	models "orbital-pathfinder/src/models/starcitizen"
	repository "orbital-pathfinder/src/repository/starcitizen"
	. "orbital-pathfinder/src/tools"
)

func ResolveCoordinates(w http.ResponseWriter, r *http.Request, coordinates form.Coordinates) (*models.Coordinates, error) {

	// Checking if the coordinates are provided
	if coordinates.Cartesian == nil && coordinates.Polar == nil && coordinates.Spheric == nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide at least one kind of coordinate", nil)
		return nil, fmt.Errorf("needs a coordinate")
	}

	ret := &models.Coordinates{}
	// Filling / Solving Coordinates
	if coordinates.Spheric != nil {
		*ret = repository.ScCoordinatesRepo.CalculateFromSpheric(models.Spheric(*coordinates.Spheric))
	} else if coordinates.Polar != nil {
		*ret = repository.ScCoordinatesRepo.CalculateFromPolar(models.Polar(*coordinates.Polar))
	} else if coordinates.Cartesian != nil {
		*ret = repository.ScCoordinatesRepo.CalculateFromCartesian(models.Cartesian(*coordinates.Cartesian))
	} else {
		return nil, fmt.Errorf("needs a coordinate")
	}
	return ret, nil
}
