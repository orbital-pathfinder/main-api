package auth

import (
	"net/http"

	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/auth")

	Router.Register("/sign-in", CheckMethod(http.MethodPost), PostSignInController)
	Router.Register("/sign-up", CheckMethod(http.MethodPost), PostSignUpController)
	Router.Register("/sign-out", CheckMethod(http.MethodPost, http.MethodGet, http.MethodDelete), IsAuthenticated, SignOutController)
}
