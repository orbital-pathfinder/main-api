package auth

import (
	"fmt"
	"net/http"

	constants "orbital-pathfinder/src/constants/security"
	"orbital-pathfinder/src/entities"
	generic "orbital-pathfinder/src/generic/api/responses"
	repository "orbital-pathfinder/src/repository/token"
	"orbital-pathfinder/src/tools"
)

func SignOutController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
	// Getting the token cookie
	token, err := r.Cookie(constants.TokenCookieName)
	if err != nil {
		fmt.Println("Missing ", constants.TokenCookieName)
		generic.SendUnauthorized(w)
		return
	}
	// Getting the authentication cookie marker
	auth, err := r.Cookie(constants.AuthenticatedCookieName)
	if err != nil {
		fmt.Println("Missing ", constants.AuthenticatedCookieName)
		generic.SendUnauthorized(w)
	}

	err = repository.TokenRepository.DeleteById(rro.Session.Token.Id)
	if err != nil {
		fmt.Println("Internal error ", err)
		tools.SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
	// Setting age to 1 (has the effect of deleting the cookie)
	token = &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    "",
		HttpOnly: constants.TokenCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.TokenCookiePath,
	}
	auth = &http.Cookie{
		Name:     constants.AuthenticatedCookieName,
		Value:    "true",
		HttpOnly: constants.AuthentifiedCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.AuthentifiedCookiePath,
	}

	// Setting cookies update
	http.SetCookie(w, token)
	http.SetCookie(w, auth)

	// Respond to client
	tools.SendApiResponse(w, http.StatusOK, "Successfully disconnected", nil)
}
