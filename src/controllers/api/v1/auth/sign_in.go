package auth

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	constants "orbital-pathfinder/src/constants/security"
	"orbital-pathfinder/src/entities"
	. "orbital-pathfinder/src/tools"

	"orbital-pathfinder/src/form/api/v1/auth"
	generic "orbital-pathfinder/src/generic/api/responses"
	repository "orbital-pathfinder/src/repository/user"
	security "orbital-pathfinder/src/security/jwt"
)

func PostSignInController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil)
		return
	}

	// Parsing json body
	formData := form.SignIn{}
	err = json.Unmarshal(body, &formData)
	if err != nil {
		generic.SendBadRequest(w)
		return
	}

	// Looking if the user is existing or not
	user, err := repository.UserRepository.FindByLoginAndPassword(formData.Login, formData.Password)
	if err != nil {
		generic.SendForbidden(w)
		return
	}

	// Making the token and creating associated cookies
	jwt := security.CreateJwt(*user)

	// expire := time.Now().Add(24 * time.Hour)
	token := &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    jwt,
		HttpOnly: constants.TokenCookieHttpOnly,
		MaxAge:   constants.TokenMaxAge,
		Path:     constants.TokenCookiePath,
	}
	auth := &http.Cookie{
		Name:     constants.AuthenticatedCookieName,
		Value:    "true",
		HttpOnly: constants.AuthentifiedCookieHttpOnly,
		MaxAge:   constants.AuthenticationMaxAge,
		Path:     constants.AuthentifiedCookiePath,
	}

	// Setting cookies
	http.SetCookie(w, token)
	http.SetCookie(w, auth)

	// Respond to client
	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), nil)
}
