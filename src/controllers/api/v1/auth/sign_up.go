package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"

	form "orbital-pathfinder/src/form/api/v1/auth"
	. "orbital-pathfinder/src/repository/user"
)

func PostSignUpController(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {

	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		tools.SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil);
		return
	}

	// Parsing json body
	form := form.SignUp{}
	err = json.Unmarshal(body, &form)
	if err != nil {
		tools.SendApiResponse(w, http.StatusBadRequest, "This is not a valid json content", nil);
		return
	}

	// Looking if the user is existing or not
	user, err := UserRepository.FindByUsernameOrMail(form.Username, form.Mail)
	if user != nil {
		fmt.Println(err, user)
		tools.SendApiResponse(w, http.StatusConflict, http.StatusText(http.StatusConflict), nil)
		return
	}

	_, err = UserRepository.CreateUserNoHashPassword(form)
	if err != nil {
		fmt.Println(err)
		tools.SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	// Respond to client
	tools.SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), nil)
	return
}
