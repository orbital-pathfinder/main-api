package me

import (
	"net/http"

	. "orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/me")

	Router.Register("", CheckMethod(http.MethodGet), IsAuthenticated, HasRole("ROLE_USER"), GetUserMe)
	Router.Register("/theme", CheckMethod(http.MethodGet, http.MethodPost), IsAuthenticated, HasRole("ROLE_USER"), SetThemeMe)
}
