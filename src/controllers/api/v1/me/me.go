package me

import (
	"net/http"
	"sort"

	"orbital-pathfinder/src/entities"
	repository "orbital-pathfinder/src/repository/user"
	. "orbital-pathfinder/src/tools"
)

func GetUserMe(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {
	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), rro.Session.Token.User)
}

func SetThemeMe(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc) {

	val := r.URL.Query().Get("theme")

	if val == "" {
		SendApiResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest) + ". Argument theme is required", nil)
		return
	}

	allowedThemes := []string{"light", "dark"}
	if index := sort.SearchStrings(allowedThemes, val); index < 0 {
		SendApiResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest) + ". Theme must be one of them", allowedThemes)
		return
	}

	rro.Session.Token.User.Theme = val
	_ = repository.UserRepository.Persist(rro.Session.Token.User)

	SendApiResponse(w, http.StatusOK, http.StatusText(http.StatusOK), nil)
}
