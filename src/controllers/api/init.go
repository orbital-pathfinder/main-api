package api

import (
	. "orbital-pathfinder/src/controllers/api/v1"
	. "orbital-pathfinder/src/router"
)

var ApiRouter *Router

func init() {
	ApiRouter = NewRouter("/api")
	ApiRouter.Use(V1Router)
}
