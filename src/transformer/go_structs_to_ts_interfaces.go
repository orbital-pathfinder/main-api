package transformer

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

type TsInterface map[string]TsType
type TsType struct {
	// If this is a primitive type of golang (Specify that type is not a user defined structure)
	Primitive  bool
	// Typescript definition of the structure
	Definition TsInterface
	// The typescript type name
	Name       string
	// If type is a typescript array
	Array      bool
}

var GoUnknownTsType = "any"

// This list defines equivalences to ignore golang types.
var GoToTsTypes = map[string]TsType{
	// Number types
	"int":     {Primitive: true, Name: "number"},
	"uint":    {Primitive: true, Name: "number"},
	"int8":    {Primitive: true, Name: "number"},
	"uint8":   {Primitive: true, Name: "number"},
	"int16":   {Primitive: true, Name: "number"},
	"uint16":  {Primitive: true, Name: "number"},
	"int32":   {Primitive: true, Name: "number"},
	"uint32":  {Primitive: true, Name: "number"},
	"int64":   {Primitive: true, Name: "number"},
	"uint64":  {Primitive: true, Name: "number"},
	"float32": {Primitive: true, Name: "number"},
	"float64": {Primitive: true, Name: "number"},

	// String types
	"string":   {Primitive: true, Name: "string"},
	"ObjectId": {Primitive: true, Name: "string"},

	// Date types
	"Time": {Primitive: true, Name: "Date"},

	// Boolean
	"bool": {Primitive: true, Name: "boolean"},

	// any
	GoUnknownTsType: {Primitive: true, Name: "any"},
}

func ToKebabCase(s string) string {
	numberSequence := regexp.MustCompile(`([a-zA-Z])(\d+)([a-zA-Z]?)`)
	s = numberSequence.ReplaceAllString(s, `$1 $2 $3`)
	s = strings.Trim(s, " ")
	n := ""
	for i, v := range s {
		// treat acronyms as words, eg for JSONData -> JSON is a whole word
		nextCaseIsChanged := false
		if i+1 < len(s) {
			next := s[i+1]
			if (v >= 'A' && v <= 'Z' && next >= 'a' && next <= 'z') || (v >= 'a' && v <= 'z' && next >= 'A' && next <= 'Z') {
				nextCaseIsChanged = true
			}
		}

		if i > 0 && n[len(n)-1] != uint8('-') && nextCaseIsChanged {
			// add underscore if next letter case type is changed
			if v >= 'A' && v <= 'Z' {
				n += "-" + string(v)
			} else if v >= 'a' && v <= 'z' {
				n += string(v) + "-"
			}
		} else if v == ' ' || v == '_' || v == '-' {
			// replace spaces/underscores with delimiters
			n += "-"
		} else {
			n = n + string(v)
		}
	}

	n = strings.ToLower(n)
	return n
}

func TypeToTs(typ reflect.Type) (TsType, error) {

	// If typ is a slice
	if typ.Kind() == reflect.Slice {
		tmp, err := TypeToTs(typ.Elem())
		if err != nil {
			panic(err)
		}
		tmp.Array = true
		return tmp, nil

		// If the typ is a struct
	} else if _, ok := GoToTsTypes[typ.Name()]; typ.Kind() == reflect.Struct && !ok {
		// Getting struct fields
		var fields []reflect.StructField
		for i := 0; i < typ.NumField(); i++ {
			fields = append(fields, typ.Field(i))
		}

		// Add the new struct definition
		GoToTsTypes[typ.Name()] = TsType{false,
			fieldsToInterface(fields),
			typ.Name() + "Interface",
			false,
		}
		return GoToTsTypes[typ.Name()], nil
		// If typ is a known type
	} else if val, ok := GoToTsTypes[typ.Name()]; ok {
		return val, nil
	}
	// If typ is not known then typ is any
	return GoToTsTypes[GoUnknownTsType], nil
}

// This function transform each fields into a TsInterface structure
func fieldsToInterface(structFields []reflect.StructField) TsInterface {
	ret := TsInterface{}

	// For each field
	for _, structField := range structFields {
		field_name := structField.Name

		// If the field has specific annotation
		if tag := structField.Tag.Get("typescript"); tag != "" {
			tagValues := strings.Split(tag, ",")
			switch strings.TrimSpace(tagValues[0]) {
			case "":
				break
			case "-":
				continue
			default:
				field_name = strings.TrimSpace(tagValues[0])
				break
			}
			// If the field has a json annotation
		} else if tag := structField.Tag.Get("json"); tag != "" {
			tagValues := strings.Split(tag, ",")
			val := tagValues[0]
			if val != "" && val != "-" {
				field_name = strings.TrimSpace(val)
			}
		}
		ret[field_name], _ = TypeToTs(structField.Type)
	}
	return ret
}

// This function transform the given model and add it to GoToTsTypes.
func transformStructFields(model reflect.Type) error {

	var fieldList []reflect.StructField

	// Getting name and all fields in the struct
	struct_name := model.Name()
	for i := 0; i < model.NumField(); i++ {
		fieldList = append(fieldList, model.Field(i))
	}

	// This function transforms the model into a TsInterface
	ts_fields := fieldsToInterface(fieldList)

	// Adding type to the known types list.
	GoToTsTypes[model.Name()] = TsType{false, ts_fields, struct_name, false}
	return nil
}

// The main function that generate the interfaces
func GenerateTsInterfaces(models ...interface{}) map[string]string {

	// Iterate over models to parse structures
	for _, model := range models {
		if err := transformStructFields(reflect.TypeOf(model)); err != nil {
			panic(err)
		}
	}

	parsed_interfaces := map[string]string{}
	// Iterate over all interfaces generated with GenerateTsInterface
	for _, interfase := range GoToTsTypes {
		// Skipping useless interfaces
		if len(interfase.Definition) <= 0 {
			continue
		}

		// Generate interface code.
		str := fmt.Sprintln("export interface ", interfase.Name+"Interface", " {")
		for name, description := range interfase.Definition {
			str += fmt.Sprint("\t", name, "\t", description.Name)
			if description.Array {
				str += fmt.Sprint("[]")
			}
			str += fmt.Sprintln(",")
		}
		str += fmt.Sprintln("}")
		filename := ToKebabCase(interfase.Name) + ".interface.ts"
		parsed_interfaces[filename] = str
	}
	return parsed_interfaces
}
