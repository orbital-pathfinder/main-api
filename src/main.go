package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"orbital-pathfinder/src/controllers/api"
	"orbital-pathfinder/src/middlewares"
	"orbital-pathfinder/src/router"
)

// https://stackoverflow.com/questions/34006446/how-would-i-go-about-inserting-a-subdocument-in-mongodb-in-golang-mgo

func main() {
	orbitalServer := mux.NewRouter()
	orbitalServer.StrictSlash(false)

	// transformer.GenerateTsInterfaces(Coordinates{})

	mainRouter := router.NewRouter("")
	mainRouter.Use(api.ApiRouter)
	mainRouter.Apply(orbitalServer)
	cors := middlewares.SetupCors()

	server := cors.Handler(orbitalServer)
	if orbitalServer == nil {
		log.Fatal("Error while creating the server")
	}

	if err := http.ListenAndServe(":8080", server); err != nil {
		log.Fatal(err)
	}
}

// db.systems.aggregate([
//
//        {
//            $lookup: {
//               from: "celestial_objects",
//               localField: "celestial_objects_id",
//               foreignField: "_id",
//               as: "celestial_objects"
//            }
//        },
//        {
//            $lookup: {
//               from: "affiliations",
//               localField: "affiliations_id",
//               foreignField: "_id",
//               as: "affiliations"
//            }
//        }
//    ])
//    .match({})
//    .project({})
//    .sort({_id:-1})
//    .limit(100)
//