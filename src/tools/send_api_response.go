package tools

import (
	"encoding/json"
	"net/http"

	"orbital-pathfinder/src/entities"
)

func SendApiResponse(w http.ResponseWriter, status int, message string, payload interface{}) {

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(status)
	resp := &entities.ApiResponse{Code: status, Message: message, Data: payload}

	r, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
	}
	_, _ = w.Write(r)
}
