package tools

import (
	"net/http"

	"orbital-pathfinder/src/entities"
)

type MdFunc func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next CtrlFunc)
type CtrlFunc func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request)

var nullcb CtrlFunc = func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request) {}

func buildChain(a MdFunc, b []MdFunc) CtrlFunc {
	c := nullcb
	if len(b) > 0 {
		c = buildChain(b[0], b[1:])
	}

	return func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request) {
		a(rro, w, r, c)
	}
}

func MiddlewareResolver(ints []MdFunc) CtrlFunc {

	return buildChain(ints[0], ints[1:])
}
