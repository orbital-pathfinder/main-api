package tools

import (
	"net/http"

	"github.com/gorilla/mux"

	models "orbital-pathfinder/src/models/starcitizen"
	repository "orbital-pathfinder/src/repository/starcitizen"
)

func ResolveSystemFromPath(w http.ResponseWriter, r *http.Request) (*models.ScSystem) {
	// Getting / Checking path vars
	pathParams := mux.Vars(r)

	// Looking for the system
	system, err := repository.ScSystemRepo.FindBySlug(pathParams["system_slug"])
	if err != nil {
		switch err.Error() {
		case "not found":
			SendApiResponse(w, http.StatusNotFound, "System '"+pathParams["system_slug"]+"' is not found", nil)
		default:
			SendApiResponse(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		}
		return nil
	}
	return system
}
