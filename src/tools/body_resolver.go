package tools

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func ResolveBody(w http.ResponseWriter, r *http.Request, data interface{}) interface{} {
	// Getting request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendApiResponse(w, http.StatusBadRequest, "You must provide a body", nil)
		return nil
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println(err)
		SendApiResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return nil
	}

	return data
}
