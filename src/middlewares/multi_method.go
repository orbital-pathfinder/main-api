package middlewares

import (
	"net/http"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

type Methods map[string][]tools.MdFunc

func MultiMethod(routes Methods) tools.MdFunc {
	return func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
		if method, ok := routes[r.Method]; ok {
			md := tools.MiddlewareResolver(method)
			md(rro, w, r)
		} else {
			tools.SendApiResponse(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed), nil)
		}
	}
}
