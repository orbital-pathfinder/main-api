package middlewares

import (
	"fmt"
	"net/http"

	"orbital-pathfinder/src/entities"
	generic "orbital-pathfinder/src/generic/api/responses"
	"orbital-pathfinder/src/tools"
)

func HasRole(roleNames ...string) tools.MdFunc {
	return func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
		s := rro.Session
		if s.Token == nil || s.Token.User == nil {
			generic.SendForbidden(w)
			return
		}
		userRoles := s.Token.User.Roles

		fmt.Println("Comparing ", roleNames, "and", userRoles)

		for _, name := range roleNames {
			for _, role := range userRoles {
				if role == name {
					next(rro, w, r)
					return
				}
			}
		}
		generic.SendForbidden(w)
	}

}
