package middlewares

import (
	"net/http"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

func HasJsonBody(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
	content := r.Header.Get("Content-Type")

	if content != "application/json" {
		tools.SendApiResponse(w, http.StatusNotAcceptable, http.StatusText(http.StatusNotAcceptable), nil)
		return
	}

	next(rro, w, r)
	return
}
