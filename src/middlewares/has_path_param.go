package middlewares

import (
	"net/http"

	"github.com/gorilla/mux"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

func HasPathParam(params []string) tools.MdFunc {
	return func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
		pathParams := mux.Vars(r)
		for _, param := range params {
			if val, ok := pathParams[param]; !ok || val == "" {
				tools.SendApiResponse(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest) + " " + param + " is required", nil)
				return
			}
		}

		next(rro, w, r)
	}
}
