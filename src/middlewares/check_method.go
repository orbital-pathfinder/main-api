package middlewares

import (
	"net/http"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

func CheckMethod(allowedMethods ...string) tools.MdFunc {
	return func(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
		for _, test := range allowedMethods {
			if r.Method == test {
				next(rro, w, r)
				return
			}
		}
		tools.SendApiResponse(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed), nil)
		return
	}
}
