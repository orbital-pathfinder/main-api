package middlewares

import (
	"net/http"
	"strconv"

	"orbital-pathfinder/src/entities"
	"orbital-pathfinder/src/tools"
)

func ShouldBePaged(defaultPage int, defaultSize int) tools.MdFunc {
	return func (rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
		paged := &entities.PagedParams{}
		page := r.URL.Query().Get("page")
		size := r.URL.Query().Get("size")

		var err error
		if page != "" && size != "" {
			if paged.Page, err = strconv.Atoi(page); err != nil {
				tools.SendApiResponse(w, http.StatusBadRequest, "'page' param should be numeric ", nil)
				return
			}
			if paged.Size, err = strconv.Atoi(size); err != nil {
				tools.SendApiResponse(w, http.StatusBadRequest, "'size' param should be numeric ", nil)
				return
			}
			if paged.Page <= 0 {
				tools.SendApiResponse(w, http.StatusBadRequest, "'page' param should be higher than 0", nil)
				return
			}
			if paged.Size <= 0 {
				tools.SendApiResponse(w, http.StatusBadRequest, "'size' param should be higher than 0", nil)
				return
			}
			rro.Paged = paged;
		} else if page == "" || size == "" {
			paged.Page = defaultPage
			paged.Size = defaultSize
		}
		next(rro, w, r)
	}
}
