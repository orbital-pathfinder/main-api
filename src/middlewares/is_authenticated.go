package middlewares

import (
	"fmt"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"

	"orbital-pathfinder/src/entities"
	. "orbital-pathfinder/src/repository/token"
	"orbital-pathfinder/src/tools"

	securityConst "orbital-pathfinder/src/constants/security"
	generic "orbital-pathfinder/src/generic/api/responses"
	security "orbital-pathfinder/src/security/jwt"
)

func IsAuthenticated(rro *entities.RequestResolvedOptions, w http.ResponseWriter, r *http.Request, next tools.CtrlFunc) {
	token, err := r.Cookie(securityConst.TokenCookieName)
	if err != nil || token.Value == "" {
		fmt.Println("Token not found", err)
		generic.SendUnauthorized(w)
		return
	}

	_, err = r.Cookie(securityConst.AuthenticatedCookieName)
	if err != nil {
		fmt.Println("Auth not found")
		generic.SendBadRequest(w)
		return
	}

	claims, err := security.ParseJwt(token.Value)
	if err != nil {
		generic.SendUnauthorized(w)
		return
	}

	uid, err := uuid.FromString(claims.ID)
	if claims.Issuer != securityConst.JWTIssuer || err != nil {
		generic.SendBadRequest(w)
		return
	}

	if claims.Expiry.Time().Unix() < time.Now().Unix() {
		fmt.Println("Expired token")
		generic.SendUnauthorized(w)
		return
	}

	test := TokenRepository.FindByUUIDTest(&uid)
	fmt.Println(test)

	tokenInfo, err := TokenRepository.FindByUUID(&uid)
	if err != nil {
		fmt.Println("Unknown token", uid)
		generic.SendUnauthorized(w)
		return
	}
	println(tokenInfo.User)
	TokenRepository.ResolveNested(1, tokenInfo)

	if claims.IssuedAt.Time().Unix() != tokenInfo.IssuedAt.Unix() {
		tools.SendApiResponse(w, http.StatusUnauthorized, "The given token has expired", nil)
		return
	}

	if claims.UserId != tokenInfo.UserId {
		tools.SendApiResponse(w, http.StatusUnauthorized, "Token error #1", nil)
		return
	}

	if claims.UserName != tokenInfo.User.Name {
		tools.SendApiResponse(w, http.StatusUnauthorized, "Token error #2", nil)
		return
	}

	rro.Session.Token = tokenInfo
	next(rro, w, r)
	return
}
